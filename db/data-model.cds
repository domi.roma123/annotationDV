//namespace matchingTool;

using
{
    Country,
    cuid,
    Currency,
    sap.common.CodeList
}
from '@sap/cds/common';

context matchingTool{

entity Chart  : cuid {
    game: String;
    player: Integer;
}

entity Orders
{
    key ID : UUID;
    nome : String;
    cognome : String;
    eta : Integer;
    Items : Composition of many Order_Items on Items.parent = $self;
}

entity Order_Items
{
    key ID : UUID;
    key parent : Association to one Orders;
    product : Association to one Products;
}

entity Products
{
    key ID : UUID;
    nome : String;
    quantity : Integer;
}

entity CheatSheetIndustry : cuid
{
    name : String
        @assert.format : '^[a-zA-Z]+$'
        @mandatory;
    country : Country
        @mandatory;
    GDP : Integer64;
    currency : Currency
        @mandatory;
    ratingEfficenty : Integer
        @assert.range : [0, 5];
    email : String(320)
        @assert.format : '^.+@[^\.].*\.[a-z]{2,}$'
        @mandatory
        @Communication.IsEmailAddress;
    phone : String
        @assert.format : '^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$'
        @mandatory
        @Communication.IsPhoneNumber;
    small_desc : String(50);
    full_desc : String;
    site : String;
    icon : LargeBinary
        @Core.MediaType : ImageType
        @UI.IsImage;
    ImageType : String
        @Core.IsMediaType;
    chartEntity : Composition of many Trend on chartEntity.parent = $self;
}

entity Trend : cuid
{
    parent : Association to one CheatSheetIndustry;
    integerValue : Integer;
    integerValueWithUoM : Integer;
    forecastValue : Integer;
    targetValue : Integer default 30;
    dimensions : Integer;
    axis_y : Integer64 default 1000000000;
    axis_x : Integer
        @assert.range : [2000, 2023];
    money : Integer64;
    areaChartToleranceUpperBoundValue : Integer default 90;
    areaChartToleranceLowerBoundValue : Integer default 80;
    areaChartDeviationLowerBoundValue : Integer default 0;
}

entity Spots : cuid
{
    pos : String;
    tooltip : String;
    type : String default 'Inactive';
    full_desc : String;
    site : String;
    text : Country;
        // @mandatory;
    email : String(320)
        @assert.format : '^.+@[^\.].*\.[a-z]{2,}$'
        @mandatory
        @Communication.IsEmailAddress;
    phone : String
        @assert.format : '^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$'
        @mandatory
        @Communication.IsPhoneNumber;
    criticality : Integer default 0
        @assert.range : [0, 5];
    Efficenty : Integer
        @readonly
        @assert.range : [0, 100];
    data: Composition of many dataCentral on data.values = $self;
}

entity dataCentral :cuid{
    values: Association to one Spots;
    date: DateTime;
    Energy : Integer;
}

}

@cds.persistence.exists 
@cds.persistence.calcview 
Entity V_INTERACTION {
key     ID: String(36)  @title: 'ID: ID' ; 
        NAME: String(5000)  @title: 'NAME: NAME' ; 
        CURRENCY_CODE: String(3)  @title: 'CURRENCY_CODE: CURRENCY_CODE' ; 
        EMAIL: String(320)  @title: 'EMAIL: EMAIL' ; 
        PHONE: String(5000)  @title: 'PHONE: PHONE' ; 
        AXIS_X: Integer  @title: 'AXIS_X: AXIS_X' ; 
        MONEY: Integer64  @title: 'MONEY: MONEY' ; 
        ID_1: String(36)  @title: 'ID_1: ID' ; 
        NAME_1: String(5000)  @title: 'NAME_1: NAME' ; 
        CURRENCY_CODE_1: String(3)  @title: 'CURRENCY_CODE_1: CURRENCY_CODE' ; 
        EMAIL_1: String(320)  @title: 'EMAIL_1: EMAIL' ; 
        PHONE_1: String(5000)  @title: 'PHONE_1: PHONE' ; 
        AXIS_X_1: Integer  @title: 'AXIS_X_1: AXIS_X' ; 
        MONEY_1: Integer64  @title: 'MONEY_1: MONEY' ; 
}