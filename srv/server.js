const proxy = require ('@sap/cds-odata-v2-adapter-proxy')
const cds = require('@sap/cds')
cds.on('bootstrap', app => app.use(proxy()))


const { WebSocket} = require("ws");

// WebSocket library
const wss = new WebSocket.Server({ noServer: true });
		

cds.on("listening", ( server ) => {
	console.log("--> listening");

	server.server.on("upgrade",function upgrade (request, socket, head) {
		console.log("--> diooooooooo");
		wss.handleUpgrade(request, socket, head, function done(ws)  {
			console.log("WSS HandleUpgrade");
			wss.emit("connection", ws, request);
		});
	

	});

	global.wss = wss;

});



module.exports = cds.server
