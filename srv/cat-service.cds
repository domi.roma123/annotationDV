using matchingTool as db from '../db/data-model';
using V_INTERACTION from '../db/data-model';

service MatchingToolService {
    @odata.draft.enabled
    entity Orders as projection on db.Orders;
    
    entity Order_Items as projection on db.Order_Items;  

    @odata.draft.enabled
    entity Products as  projection on db.Products;

    @odata.draft.enabled
    entity CheatSheetIndustry as projection on db.CheatSheetIndustry;

    @odata.draft.enabled
    entity Spots as projection on db.Spots; 

    // @cds.redirection.target:true
    // entity SpotsCall as projection on db.Spots;L
    
    function sleep() returns Boolean;
    @readonly
    entity V_Interaction as projection on V_INTERACTION;

    @odata.draft.enabled
    entity Chart as  projection on db.Chart;
}