sap.ui.define([
    "sap/ui/test/opaQunit"
], function (opaTest) {
    "use strict";

    var Journey = {
        run: function() {
            QUnit.module("First journey");

            opaTest("Start application", function (Given, When, Then) {
                Given.iStartMyApp();

                Then.onTheCheatSheetIndustryList.iSeeThisPage();

            });


            opaTest("Navigate to ObjectPage", function (Given, When, Then) {
                // Note: this test will fail if the ListReport page doesn't show any data
                When.onTheCheatSheetIndustryList.onFilterBar().iExecuteSearch();
                Then.onTheCheatSheetIndustryList.onTable().iCheckRows();

                When.onTheCheatSheetIndustryList.onTable().iPressRow(0);
                Then.onTheCheatSheetIndustryObjectPage.iSeeThisPage();

            });

            opaTest("Teardown", function (Given, When, Then) { 
                // Cleanup
                Given.iTearDownMyApp();
            });
        }
    }

    return Journey;
});