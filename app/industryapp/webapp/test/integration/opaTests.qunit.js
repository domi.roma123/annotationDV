sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'industryapp/test/integration/FirstJourney',
		'industryapp/test/integration/pages/CheatSheetIndustryList',
		'industryapp/test/integration/pages/CheatSheetIndustryObjectPage'
    ],
    function(JourneyRunner, opaJourney, CheatSheetIndustryList, CheatSheetIndustryObjectPage) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('industryapp') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onTheCheatSheetIndustryList: CheatSheetIndustryList,
					onTheCheatSheetIndustryObjectPage: CheatSheetIndustryObjectPage
                }
            },
            opaJourney.run
        );
    }
);