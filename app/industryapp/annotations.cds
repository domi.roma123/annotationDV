using MatchingToolService as service from '../../srv/cat-service';


using from '@sap/cds/common';
using from '../../db/data-model';

annotate service.CheatSheetIndustry with @(UI.HeaderInfo: {
    TypeName      : 'Industry',
    TypeNamePlural: 'Industrie',
    Title         : {Value: `{name} `},
    Description   : {Value: `{small_desc}`},


});


annotate service.CheatSheetIndustry with @(
    UI.DataPoint #Rating: {
        Value        : ratingEfficenty,
        TargetValue  : 5,
        Visualization: #Rating,
    },
    UI.LineItem         : [
        {
            $Type: 'UI.DataField',
            Label: 'Name',
            Value: name,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Country',
            Value: country_code,
        },
        {
            $Type                    : 'UI.DataField',
            Label                    : 'Annual revenue',
            Criticality              : {$edmJson: {$If: [
                {$Lt: [
                    {$Path: 'GDP'},
                    0
                ]},
                1,
                3
            ]}},
            CriticalityRepresentation: #WithoutIcon,
            Value                    : GDP,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Currency Code',
            Value: currency_code,
        },
        {
            $Type : 'UI.DataFieldForAnnotation',
            Label : 'Rating',
            Target: '@UI.DataPoint#Rating'
        },
    ]
);

annotate service.CheatSheetIndustry with @(
    UI.FieldGroup #GeneratedGroup1: {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Label: 'Name',
                Value: name,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Country',
                Value: country_code,

            },
            {
                $Type: 'UI.DataField',
                Label: 'Annual revenue',
                Value: GDP,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Currency Code',
                Value: currency_code,
            },
            {
                $Type : 'UI.DataFieldForAnnotation',
                Label : 'Rating',
                Target: '@UI.DataPoint#Rating'
            },
            {

                $Type: 'UI.DataField',
                Value: email,
                Label: 'email',
            },
            {
                $Type: 'UI.DataField',
                Value: phone,
                Label: 'phone',
            }, 
          {
                $Type        : 'UI.DataFieldWithUrl',
                Value        : ' Link Site',
                Url          : site,
                IconUrl      : 'sap-icon://broken-link',
                ![@UI.Hidden]: {$edmJson: {$If: [
                    {$Eq: [
                        {$Path: 'IsActiveEntity'},
                        true
                    ]},
                    false,
                    true
                ]}},

            },
            {
                $Type        : 'UI.DataField',
                Value        : site,
                Label        : 'Link Site',
                ![@UI.Hidden]: {$edmJson: {$If: [
                    {$Eq: [
                        {$Path: 'IsActiveEntity'},
                        false
                    ]},
                    false,
                    true
                ]}},

            },

           


        ],

    },
    UI.Facets                     : [
        {
            $Type : 'UI.ReferenceFacet',
            ID    : 'GeneratedFacet1',
            Label : 'Mandatory Information',
            Target: '@UI.FieldGroup#GeneratedGroup1',
        },
        {
            $Type : 'UI.ReferenceFacet',
            Label : 'Optional Information',
            ID    : 'OptionalInformation',
            Target: '@UI.FieldGroup#OptionalInformation',
        },
        {
            $Type : 'UI.ReferenceFacet',
            Label : 'Industry Annual Increment from 2015',
            ID : 'IndustryAnnualIncrementfrom2015',
            Target : 'chartEntity/@UI.PresentationVariant#chartSection',
        },
        
    ]
);


annotate service.CheatSheetIndustry with {
    country @Common.Text: country.name
};

annotate service.CheatSheetIndustry with {
    country @Common.ValueListWithFixedValues: true
};

annotate service.Countries with {
    code @Common.Text: name
};


annotate service.CheatSheetIndustry with {
    country @Common.ValueList: {
        $Type         : 'Common.ValueListType',
        CollectionPath: 'Countries',
        Parameters    : [
            {
                $Type            : 'Common.ValueListParameterInOut',
                LocalDataProperty: country_code,
                ValueListProperty: 'code',
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'descr',
            },

        ],
    }


};


annotate service.CheatSheetIndustry with {
    currency @Common.ValueListWithFixedValues: false
};

annotate service.CheatSheetIndustry with {
    currency @Common.ValueList: {
        $Type         : 'Common.ValueListType',
        CollectionPath: 'Currencies',
        Parameters    : [
            {
                $Type            : 'Common.ValueListParameterInOut',
                LocalDataProperty: currency_code,
                ValueListProperty: 'code',
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'descr',
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'name',
            },
            {
                $Type            : 'Common.ValueListParameterDisplayOnly',
                ValueListProperty: 'symbol',
            },
        ],
    }
};


annotate service.CheatSheetIndustry with @(Communication.Contact #contact: {
    $Type: 'Communication.ContactType',
    fn   : name,
    title: name,
    org  : country.name,
});

annotate service.CheatSheetIndustry with @(
    UI.HeaderFacets        : [{
        $Type : 'UI.ReferenceFacet',
        Label : 'Information',
        ID    : 'Contatti',
        Target: '@UI.FieldGroup#Contatti',
    },
    
    {

        $Type : 'UI.ReferenceFacet',
        ID    : 'integerValue',
        Target: '@UI.Chart#columnChart',

    }
        ],
    UI.FieldGroup #Contatti: {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Value: full_desc,
            },
            {
                $Type : 'UI.DataFieldForAnnotation',
                Target: '@Communication.Contact#contact2',
                Label : 'Contact utility',
            },
            {
                $Type        : 'UI.DataField',
                Value        : site,
                ![@UI.Hidden]: true,
            },
        ],

    },

);

annotate service.CheatSheetIndustry with @(Communication.Contact #contact1: {
    $Type: 'Communication.ContactType',
    fn   : email,
});

annotate service.CheatSheetIndustry with @(Communication.Contact #contact2: {
    $Type: 'Communication.ContactType',
    fn   : 'click on me',
    tel  : [{
        $Type: 'Communication.PhoneNumberType',
        type : #work,
        uri  : phone,
    }, ],
    email: [{
        $Type  : 'Communication.EmailAddressType',
        type   : #work,
        address: email,
    }, ],
    photo: icon,


});

annotate service.CheatSheetIndustry with {
    full_desc @UI.MultiLineText: true
};

annotate service.CheatSheetIndustry with @(UI.FieldGroup #Graphic: {
    $Type: 'UI.FieldGroupType',
    Data : [{
        $Type: 'UI.DataField',
        Value: ID,
        Label: 'ID',
    }, ],
});

annotate service.CheatSheetIndustry with @(UI.FieldGroup #OptionalInformation: {
    $Type: 'UI.FieldGroupType',
    Data : [
        {
            $Type: 'UI.DataField',
            Value: small_desc,
            Label: 'Small Description',

        },
        {
            $Type: 'UI.DataField',
            Value: full_desc,
            Label: 'Full description',
        },
        {
            $Type: 'UI.DataField',
            Value: icon,
            Label: 'Logo',

        },
        {
            $Type: 'UI.DataField',
            Value: icon,
            Label: 'Logo',

        },
        {
            $Type: 'UI.DataField',
            Value: {$edmJson: {
                $Apply   : [
                    'L azienda ',
                    {Path: 'name'},
                    ' ha fatturato ',
                    {Path: 'GDP'},

                ],
                $Function: 'odata.concat',
            }, },
        },
    ],
});

annotate service.CheatSheetIndustry with @(
   
        UI.Chart #columnChart: {

            //Search-Term: #microChartColumn

            Title            : 'Rating agency',
            Description      : '',
            ChartType        : #Column,
            Measures         : [ratingEfficenty],
            Dimensions       : [name],

            MeasureAttributes: [

            {

                $Type    : 'UI.ChartMeasureAttributeType',
                Measure  : ratingEfficenty,
                Role     : #Axis1,
                DataPoint: '@UI.DataPoint#dataPointForChart',

            }

            ],
            DynamicMeasures : []

        },
    



);


annotate service.CheatSheetIndustry with @(

    UI.DataPoint #dataPointForChart: {

        
        Value      : ratingEfficenty,
        Criticality: {$edmJson :
                        {$If :
                            [{$Lt :
                                [{$Path : 'ratingEfficenty'},2]},
                                1,{$If :
                            [{$Gt :
                                [{$Path : 'ratingEfficenty'},4]},
                                3, 2]}]}},

    },

    // ![@UI.Criticality]             : criticality_code,


);

annotate service.CheatSheetIndustry with @(UI.Identification: []);


annotate service.CheatSheetIndustry with @(
    UI.FieldGroup #Industrycomparision : {
        $Type : 'UI.FieldGroupType',
        Data : [
        ],
    }
);

/// sezione grafica

annotate service.Trend with @(
    Aggregation.ApplySupported : {
        Transformations          : [
            'aggregate',
            'topcount',
            'bottomcount',
            'identity',
            'concat',
            'groupby',
            'filter',
            'expand',
            'search',
            
        ],
        Rollup                   : #None,
        PropertyRestrictions     : true,
        GroupableProperties : [
            dimensions,integerValue,money,axis_x
            
        ],
        AggregatableProperties : [
            {
                $Type : 'Aggregation.AggregatablePropertyType',
                Property : money,
                RecommendedAggregationMethod : 'average',
                SupportedAggregationMethods : [
                    'min',
                    'max',
                    'average',
                    'countdistinct'
                ],
            },
        ],
    }
);

annotate service.Trend with @(
    Analytics.AggregatedProperties : [
    {
        Name                 : 'minAmount',
        AggregationMethod    : 'min',
        AggregatableProperty : 'money',
        ![@Common.Label]     : 'Minimal Net Amountsssss'
    },
    {
        Name                 : 'maxAmount',
        AggregationMethod    : 'max',
        AggregatableProperty : 'money',
        ![@Common.Label]     : 'Maximal Net Amount'
    },
    {
        Name                 : 'avgAmount',
        AggregationMethod    : 'average',
        AggregatableProperty : 'money',
        ![@Common.Label]     : 'Average Net Amount'
    },
       {
        Name                 : 'countdistinct',
        AggregationMethod    : 'countdistinct',
        AggregatableProperty : 'money',
        ![@Common.Label]     : 'countdistinct'
    }
    ],
);
annotate service.Trend with @(
    Analytics.AggregatedProperty #money_undefined : {
        $Type : 'Analytics.AggregatedPropertyType',
        Name : 'money_undefined',
        AggregatableProperty : money,
        AggregationMethod : 'average',
        ![@Common.Label] : 'money',
    },
    UI.Chart #chartSection : {
        $Type : 'UI.ChartDefinitionType',
        ChartType : #Line,
        Dimensions : [
            axis_x,
        ],
        DynamicMeasures : [
            '@Analytics.AggregatedProperty#money_undefined',
        ],
    }
);
annotate service.Trend with @(
    UI.PresentationVariant #chartSection : {
        $Type : 'UI.PresentationVariantType',
        Visualizations : [
            '@UI.Chart#chartSection',
        ],
        SortOrder : [
            {
                $Type : 'Common.SortOrderType',
                Descending : false,
                Property : axis_x,
            },
        ],
    }
);
annotate service.Trend with {
    axis_x @Common.Label : 'year'
};
