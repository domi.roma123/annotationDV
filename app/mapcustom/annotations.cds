using MatchingToolService as service from '../../srv/cat-service';


using from '../../db/data-model';

annotate service.Spots with @(
    UI.Facets                   : [{
        $Type : 'UI.ReferenceFacet',
        Label : 'Nuclear Central info',
        ID    : 'DetailCountry',
        Target: '@UI.FieldGroup#DetailCountry',
    },
        {
            $Type : 'UI.ReferenceFacet',
            Label : ' Measurements',
            ID : 'Measurements',
            Target : 'data/@UI.LineItem#Measurements',
            ![@UI.Hidden] : { $edmJson : {$If : [ { $Eq : [ { $Path : 'IsActiveEntity'}, false ]}, false, true ]}},
        },
        {
            $Type : 'UI.ReferenceFacet',
            Label : 'Energy product',
            ID : 'Energyproduct',
            Target : 'data/@UI.PresentationVariant#chartSection',
        }, ],
    UI.FieldGroup #DetailCountry: {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Value: text_code,
                Label: 'Country',
            },
            {
                $Type: 'UI.DataField',
                Value: tooltip,
                Label: 'City',
            },
            {
                $Type: 'UI.DataField',
                Value: pos,
                Label: 'Geographic coordinates',
            },
            {
                $Type: 'UI.DataField',
                Value: email,
                Label: 'Email address',
            },
            {
                $Type: 'UI.DataField',
                Value: phone,
                Label: ' telephone',
            },
        ],
    }
);

annotate service.Spots with @(UI.HeaderInfo: {
    // ImageUrl : 'https://c8.alamy.com/comp/2HWJ52H/iaea-international-atomic-energy-agency-symbol-icon-2HWJ52H.jpg',
    TypeName      : '',
    TypeNamePlural: '',

    Description   : {
        $Type: 'UI.DataField',
        Value: 'International atomic energy agency ',
    },
    Title         : {
        $Type: 'UI.DataField',
        Value: 'I.A.E.A.',
    },

});

annotate service.Spots with @(
    UI.HeaderFacets         : [
        {
            $Type : 'UI.ReferenceFacet',
            Label : 'First info',
            ID    : 'Firstinfo',
            Target: '@UI.FieldGroup#Firstinfo',
        },
        {
            $Type : 'UI.ReferenceFacet',
            ID    : 'Efficenty',
            Target: '@UI.DataPoint#progress',
        },
        {
            $Type : 'UI.ReferenceFacet',
            ID    : 'criticality',
            Target: '@UI.DataPoint#rating',
        },

    ],
    UI.FieldGroup #Firstinfo: {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Value: full_desc,
                Label: 'full_desc',
            },
            {
                $Type  : 'UI.DataFieldWithUrl',
                Value  : ' Link Site',
                Url    : site,
                IconUrl: 'sap-icon://broken-link',

            },
        ],
    }
);

annotate service.Spots with @(UI.DataPoint #progress: {
    $Type        : 'UI.DataPointType',
    Value        : Efficenty,
    Title        : 'Efficenty',
    TargetValue  : 100,
    Visualization: #Progress ,
    Criticality  : {$edmJson: {$If: [
        {$Lt: [
            {$Path: 'Efficenty'},
            25
        ]},
        1,
        {$If: [
            {$Gt: [
                {$Path: 'Efficenty'},
                80
            ]},
            3,
            2
        ]}
    ]}},
});

annotate service.Spots with @(UI.DataPoint #rating: {
    $Type        : 'UI.DataPointType',
    Value        : criticality,
    Title        : 'Rating',
    TargetValue  : 5,
    Visualization: #Rating,
   
});
annotate service.dataCentral with @(
    UI.LineItem #Measurements : [
        {
            $Type : 'UI.DataField',
            Value : date,
            Label : 'Date',
        },{
            $Type : 'UI.DataField',
            Value : Energy,
            Label : 'Energy',
        },]
);
annotate service.Spots with {
    full_desc @Common.FieldControl : #ReadOnly
};
annotate service.Spots with {
    text @Common.ValueListWithFixedValues : true
};



// chart
annotate service.dataCentral with @(
    Aggregation.ApplySupported : {
        Transformations          : [
            'aggregate',
            'topcount',
            'bottomcount',
            'identity',
            'concat',
            'groupby',
            'filter',
            'expand',
            'search',
            
        ],
        Rollup                   : #None,
        PropertyRestrictions     : true,
        GroupableProperties : [
            Energy,date
            
        ],
        AggregatableProperties : [
            {
                $Type : 'Aggregation.AggregatablePropertyType',
                Property : Energy,
                RecommendedAggregationMethod : 'average',
                SupportedAggregationMethods : [
                    'min',
                    'max',
                    'average',
                    'countdistinct'
                ],
            },
        ],
    }
);

annotate service.dataCentral with @(
    Analytics.AggregatedProperties : [
    {
        Name                 : 'minAmount',
        AggregationMethod    : 'min',
        AggregatableProperty : 'Energy',
        ![@Common.Label]     : 'Minimal Net Amountsssss'
    },
    {
        Name                 : 'maxAmount',
        AggregationMethod    : 'max',
        AggregatableProperty : 'Energy',
        ![@Common.Label]     : 'Maximal Net Amount'
    },
    {
        Name                 : 'avgAmount',
        AggregationMethod    : 'average',
        AggregatableProperty : 'Energy',
        ![@Common.Label]     : 'Average Net Amount'
    },
       {
        Name                 : 'countdistinct',
        AggregationMethod    : 'countdistinct',
        AggregatableProperty : 'Energy',
        ![@Common.Label]     : 'countdistinct'
    }
    ],
);

annotate service.dataCentral with @(
    Analytics.AggregatedProperty #Energy_undefined : {
        $Type : 'Analytics.AggregatedPropertyType',
        Name : 'Energy_average',
        AggregatableProperty : Energy,
        AggregationMethod : 'average',
        ![@Common.Label] : 'Energy average',
    },
    UI.Chart #chartSection : {
        $Type : 'UI.ChartDefinitionType',
        ChartType : #Line,
        Dimensions : [
            date,
        ],
        DynamicMeasures : [
            '@Analytics.AggregatedProperty#Energy_undefined',
        ],
    }
);
annotate service.dataCentral with @(
    UI.PresentationVariant #chartSection : {
        $Type : 'UI.PresentationVariantType',
        Visualizations : [
            '@UI.Chart#chartSection',
        ],
        SortOrder : [
            {
                $Type : 'Common.SortOrderType',
                Descending : false,
                Property : date,
            },
        ],
    }
);
annotate service.dataCentral with {
    date @Common.Label : 'Date registration measurements'
};
annotate service.dataCentral with {
    Energy @Common.Label : 'energy registration '
};
