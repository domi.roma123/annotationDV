sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'mapcustom/test/integration/FirstJourney',
		'mapcustom/test/integration/pages/SpotsMain'
    ],
    function(JourneyRunner, opaJourney, SpotsMain) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('mapcustom') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onTheSpotsMain: SpotsMain
                }
            },
            opaJourney.run
        );
    }
);