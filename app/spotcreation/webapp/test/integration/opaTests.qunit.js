sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'spotcreation/test/integration/FirstJourney',
		'spotcreation/test/integration/pages/SpotsList',
		'spotcreation/test/integration/pages/SpotsObjectPage'
    ],
    function(JourneyRunner, opaJourney, SpotsList, SpotsObjectPage) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('spotcreation') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onTheSpotsList: SpotsList,
					onTheSpotsObjectPage: SpotsObjectPage
                }
            },
            opaJourney.run
        );
    }
);