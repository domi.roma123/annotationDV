using MatchingToolService as service from '../../srv/cat-service';

annotate service.Spots with @(
    UI.LineItem : [
        {
            $Type : 'UI.DataField',
            Label : 'pos',
            Value : pos,
        },
        {
            $Type : 'UI.DataField',
            Label : 'tooltip',
            Value : tooltip,
        },
        {
            $Type : 'UI.DataField',
            Label : 'type',
            Value : type,
        },
        {
            $Type : 'UI.DataField',
            Label : 'full_desc',
            Value : full_desc,
        },
        {
            $Type : 'UI.DataField',
            Label : 'site',
            Value : site,
        },
    ]
);
