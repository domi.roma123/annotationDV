sap.ui.define(
    [
        'sap/fe/core/PageController',
	    "sap/m/MessageToast",
        "sap/ui/core/Fragment"
    ],
    function(PageController, MessageToast) {
        'use strict';

        return PageController.extend('map.ext.main.Main', {
            
            
            onInit: function() {
          
            },
    
            onPressLegend: function() {
                if (this.byId("vbi").getLegendVisible() == true) {
                    this.byId("vbi").setLegendVisible(false);
                    this.byId("btnLegend").setTooltip("Show legend");
                } else {
                    this.byId("vbi").setLegendVisible(true);
                    this.byId("btnLegend").setTooltip("Hide legend");
                }
            },
    
            onPressResize: function() {
                if (this.byId("btnResize").getTooltip() == "Minimize") {
                    this.byId("btnResize").setTooltip("Maximize");
                } else {
                    this.byId("vbi").maximize();
                    this.byId("btnResize").setTooltip("Minimize");
                }
            },
    
            // onRegionClick: function(e) {
            //     MessageToast.show("onRegionClick " + e.getParameter("code"));
            // },
    
            // onRegionContextMenu: function(e) {
            //     MessageToast.show("onRegionContextMenu " + e.getParameter("code"));
            // },
    
            // onClickItem: function(evt) {
            //     MessageToast.show("onClick");
                
            // },
            
            // onContextMenuItem: function(evt) {
            //     MessageToast.show("onContextMenu");
            // },
            
            onClickSpot: function(evt)  {
                const routing = this.getExtensionAPI().routing
                routing.navigate(evt.getSource().getBindingContext())
            },
            
            
        });
    }
);
