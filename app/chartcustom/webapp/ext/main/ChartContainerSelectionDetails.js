sap.ui.define([ 'sap/ui/core/Item', 'sap/m/MessageToast', 'sap/ui/layout/VerticalLayout', 'sap/m/Button', 'sap/m/Text', 'sap/m/Image' ],
	function(Item, MessageToast, VerticalLayout, Button, Text, Image) {
	"use strict";

	return {
		/**
		 * Fills the SelectionDetails Popover with actions.
		 *
		 * @param {Object} chartContainerContent The instance of the ChartContainerContent used
		 */
		_initializeSelectionDetails: function(chartContainerContent) {
			var oSelectionDetails = chartContainerContent.getSelectionDetails();
			oSelectionDetails.addAction(
				new Item({
						key: "Action 1",
						text: "First List Action"
					}
				)
			);
			oSelectionDetails.addAction(
				new Item({
						key: "Action 2",
						text: "Second List Action"
					}
				)
			);
			oSelectionDetails.attachBeforeOpen(this._onSelectionDetailsBeforeOpen, this);
			oSelectionDetails.attachActionPress(function(oEvent) {
				MessageToast.show(oEvent.getParameter("action").getText() + " is pressed.\nAt the moment of press " + oEvent.getParameter("items").length + " items were selected.");
			});
			oSelectionDetails.attachNavigate(this._onSelectionDetailsNavigate, this);
		},

		/**
		 * Fills the SelectionDetails items with actions.
		 *
		 * @param {sap.ui.base.Event} event The SAPUI5 event instance
		 */
		_onSelectionDetailsBeforeOpen: function(event) {
			var aItems = event.getSource().getItems();
			for (var i = 0; i < aItems.length; i++) {
				aItems[i].setEnableNav(true);
				aItems[i].addAction(new Item({
					key: i,
					text: "Item Action " + i
				}));
			}
		},

		/**
		 * Fills the contents of the next pages.
		 *
		 * @param {Object} chartContainerContent The instance of the ChartContainerContent used
		 * @param {sap.ui.base.Event} event The SAPUI5 event object.
		 */
		_onSelectionDetailsNavigate: function(event) {
			if (event.getParameter("direction") === "back") {
				return;
			}
			var oSelectionDetails = event.getSource();
			var oText = new Text({
				text: "La prima console prodotta commercialmente è la Magnavox Odyssey, uscita in Nord America nel 1972, sebbene il suo ideatore Ralph Baer stesse elaborando prototipi già dagli anni '60." +
                "Il concetto di console ha raggiunto maggiore celebrità con l'uscita della versione casalinga del gioco Pong (1975)."+
                "Dagli anni ottanta molte industrie si avvicendano nella produzione di console sempre più potenti e capaci di eseguire programmi di grande complessità."+
                "Complice anche l'enorme diffusione dei computer casalinghi, diverse aziende come Atari, SEGA e Nintendo diventano protagoniste della diffusione delle console."+
                "Soprattutto quest'ultima, che nel 1983 pubblica il Family Computer (noto anche come Famicom) in Giappone."+
                "Questa console supporta figure bidimensionali ad alta risoluzione e in generale una grafica più definita e colorata rispetto alle console concorrenti."+
                "Inoltre i giochi del Famicom hanno una longevità superiore rispetto ai giochi passati e una grafica maggiormente dettagliata."+
                "La console è un successo in patria, grazie anche alle conversioni di videogiochi arcade da sala giochi come Donkey Kong; per questo motivo nel 1985 la Nintendo decide di entrare nel mercato nordamericano."
			});
			var VBox2 = new VerticalLayout({
				content: [
					new Image({
						src: "https://ilcorpoinunastanza.blogautore.espresso.repubblica.it/files/2019/02/videogiochi.jpg",
                        width:"350px"
					}),
					new Text({
						text: "More information can be displayed here."
					})
				]
			}).addStyleClass("sapUiSmallMargin");
			var oButton = new Button({
				text: "More Details",
				press: function() {
					oSelectionDetails.navTo("Second Level of Navigation", VBox2);
				}
			});
			var oVBox1 = new VerticalLayout({
				content: [ oText, oButton ]
			}).addStyleClass("sapUiSmallMargin");
			oSelectionDetails.navTo("First Level of Navigation", oVBox1);
		}
	};
});
