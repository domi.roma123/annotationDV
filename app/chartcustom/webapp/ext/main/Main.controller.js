sap.ui.define([
    'sap/fe/core/PageController',
    'sap/suite/ui/commons/ChartContainerContent',
    'sap/viz/ui5/controls/VizFrame',
    'sap/viz/ui5/data/FlattenedDataset',
    'sap/viz/ui5/controls/common/feeds/FeedItem',
    'sap/viz/ui5/controls/Popover',
    './ChartContainerSelectionDetails'
], function (PageController, ChartContainerContent, VizFrame, FlattenedDataset, FeedItem, Popover, ChartContainerSelectionDetails) {
    'use strict'

        return PageController.extend('custom.ext.main.Main', {

            _constants: {
                vizFrame: {
                    id: "idVizFrame",
                    dataset: {
                        dimensions: [{
                            name: 'GameList',
                            value: "{ID}",
                            displayValue: "{game}"
                        }],
                        measures: [{
                            name: 'Player',
                            value: '{player}'
                        }],
                        data: {
                            path: '/Chart',
                        }
                    },
                    type: "column",
                    uiConfig: {
                        applicationSet: 'fiori'
                    },
                    customElements: '',
                    height: '500px',
                    width: '100%',
                    feedItems: [{
                        "uid": "primaryValues",
                        "type": "Measure",
                        "values": ['Player'],
                    }, {
                        "uid": "axisLabels",
                        "type": "Dimension",
                        "values": ['GameList']
                    }]
                },
                table: {
                    icon: 'sap-icon://table-chart',
                    title: 'Table',
                    itemBindingPath: '/Chart',
                    columnLabelTexts: ['Game', 'Player'],
                    templateCellLabelTexts: ['{game}','{player}']
                }
            },
            _state: { //stato delle componenti dell' applicazione
                chartContainer: null, //contenitore del grafico
                chartContent: null,   //contenuto del grafico
                tableContent: null,   //contenuto del grafico
                isChartShown: true    //visibilità all' avvio
            },
    
            onInit: function () {
                //assegnamento degli elementi allo stato dell' applicazione
                this._state.chartContainer = this.getView().byId('chartContainer');
                this._state.chartContent = this._createChartContent();
                this._state.tableContent = this._createTableContent();
                this._state.chartContainer.addContent(this._state.chartContent);
            
                //chiamata della detail
                var oContent = this._state.chartContainer.getContent()[0];
                ChartContainerSelectionDetails._initializeSelectionDetails(oContent);
                this._state.chartContainer.updateChartContainer();
            },
            
    
    
            //assegnazione del contenuto del chart al contenitore
            _createChartContent: function () {
                var oChartContent = new ChartContainerContent({
                    icon: 'sap-icon://vertical-bar-chart',
                    title: 'Sales Chart',
                    content: new VizFrame({
                        id: 'vizFrame',
                        uiConfig: this._constants.vizFrame.uiConfig,
                        height: this._constants.vizFrame.height,
                        width: this._constants.vizFrame.width,
                        vizType: this._constants.vizFrame.type,
                        dataset: new FlattenedDataset(this._constants.vizFrame.dataset),
                        feeds: this._constants.vizFrame.feedItems.map(function (feed) {
                            return new FeedItem(feed);
                        }),
                        
                    }),
                });
    
                //creazione del popover
                var oVizFrame = oChartContent.getContent();
                oVizFrame.attachSelectData(function (oEvent) {
                    var oSelectedItem = oEvent.getParameter("data")[0].data;
                    var sItemId = oSelectedItem.GameList;
    
                    var oPopover = new Popover();
                    oPopover.connect(oVizFrame.getVizUid());
    
                    oPopover.setActionItems([{
                        type: 'action',
                        text: 'Navigate to details' ,
                        press: function () {
                            this.getExtensionAPI().routing.navigateToRoute("ChartObjectPage", {
                                ChartKey: sItemId,
                            });
                        }.bind(this)
                    }]);
                }.bind(this));
    
                return oChartContent;
            },
            
            //gestione dello switch tra Chart e List
            onPressIconTable: function () {
                this._state.isChartShown = !this._state.isChartShown;
    
                if (this._state.isChartShown) {
                    this._state.chartContainer.removeContent(this._state.tableContent);
                    this._state.chartContainer.addContent(this._state.chartContent);
                } else {
                    this._state.chartContainer.removeContent(this._state.chartContent);
                    this._state.chartContainer.addContent(this._state.tableContent);
                }
            },
    
            //creazione della Table per lo switch
            _createTableContent: function () {
                var oTableContent = new ChartContainerContent({
                    icon: this._constants.table.icon,
                    title: this._constants.table.title,
                    content: new sap.m.Table({
                        width: '100%',
                        fixedLayout: true,
                        columns: this._constants.table.columnLabelTexts.map(function (columnText) {
                            return new sap.m.Column({
                                header: new sap.m.Label({
                                    text: columnText
                                })
                            });
                        }),
                        items: {//definizione del template
                            path: this._constants.table.itemBindingPath,
                            template: new sap.m.ColumnListItem({
                                type: "Navigation",
                                cells: this._constants.table.templateCellLabelTexts.map(function (cellLabelText) {
                                    return new sap.m.Text({
                                        text: cellLabelText
                                    });
                                }),
                                press: function (oEvent) {//press delle righe della list con navigazione
                                    var oItem = oEvent.getSource();
                                    var oBindingContext = oItem.getBindingContext();
                                    var sItemId = oBindingContext.getProperty("ID");
    
                                    this.getExtensionAPI().routing.navigateToRoute("ChartObjectPage", {
                                        ChartKey: sItemId
                                    });
                                }.bind(this)
                            })
                        }
                    })
                });
    
                return oTableContent;
            },
        });
    });
    