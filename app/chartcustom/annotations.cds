using MatchingToolService as service from '../../srv/cat-service';

annotate service.Chart with @(
    UI.Facets : [
        {
            $Type : 'UI.ReferenceFacet',
            Label : 'Gam Info',
            ID : 'GamInfo',
            Target : '@UI.FieldGroup#GamInfo',
        },
    ],
    UI.FieldGroup #GamInfo : {
        $Type : 'UI.FieldGroupType',
        Data : [
            {
                $Type : 'UI.DataField',
                Value : game,
                Label : 'game',
            },{
                $Type : 'UI.DataField',
                Value : player,
                Label : 'player',
            },],
    }
);
annotate service.Chart with @(
    UI.DataPoint #player : {
        $Type : 'UI.DataPointType',
        Value : player,
        Title : 'player',
    },
    UI.HeaderFacets : [
        {
            $Type : 'UI.ReferenceFacet',
            ID : 'player',
            Target : '@UI.DataPoint#player',
        },
    ]
);
annotate service.Chart with @(
    UI.HeaderInfo : {
        TypeName : 'Game Chart',
        TypeNamePlural : 'Game Chart',
    }
);
